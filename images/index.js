export const images = {
    close: require('./close.png'),
    marker: require('./marker.png'),
    option: require('./option.png'),
    phone: require('./phone.png'),
    star: require('./star.png'),
    clock: require('./clock.png')
}