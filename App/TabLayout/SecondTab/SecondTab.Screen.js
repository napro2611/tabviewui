import React, { Component } from 'react'
import { Dimensions, Text, View, SafeAreaView, Image, StyleSheet } from 'react-native'
import _ from 'lodash'
import { images } from '../../../images';
const { width, height } = Dimensions.get('window')

export default class SecondTabScreen extends Component {

  constructor(props) {
    super(props)
  }

  componentWillMount() {
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true
  }

  render() {
    const { item } = this.props
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={styles.header}>
          <Image
            source={images.close}
            style={styles.icon}
          />
          <View style={styles.headerInfo}>
            <Image
              style={styles.avatar}
              source={{ uri: 'https://znews-stc.zdn.vn/static/topic/person/billgates.jpg' }}
            />
            <View style={styles.infoContainer}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={styles.name}>Bill Gates</Text>
                <Text>4.8</Text>
                <Image
                  source={images.star}
                  style={styles.star}
                />
              </View>
              <Text>A1 Ausie cook  |  43 Reviewers</Text>
            </View>
          </View>
          <Image
            source={images.option}
            style={styles.iconRight}
          />
        </View>
        <View style={styles.lineSeparator} />
        <View style={styles.item}>
          <Image
            style={{ width: 48, height: 48, borderRadius: 24 }}
            source={{ uri: 'https://fiverr-res.cloudinary.com/images/t_medium7,q_auto,f_auto/gigs/28185342/original/137f6cedd439d16a41741078e859c1a36926311d/create-a-cartoon-character.jpg' }}
          />
          <View style={styles.itemText}>
            <View style={styles.itemHeader}>
              <Text style={styles.text}>Fixle</Text>
              <Text style={styles.hour}>09:05PM</Text>
            </View>
            <Text style={{ marginRight: 16, marginTop: 12 }}>We have receive your booking. I will be back shortly to introduce your assigned professional.</Text>
          </View>
        </View>
        <View style={{ height: 20 }} />
        <View style={styles.item}>
          <Image
            style={{ width: 48, height: 48, borderRadius: 24 }}
            source={{ uri: 'https://fiverr-res.cloudinary.com/images/t_medium7,q_auto,f_auto/gigs/28185342/original/137f6cedd439d16a41741078e859c1a36926311d/create-a-cartoon-character.jpg' }}
          />
          <View style={styles.itemText}>
            <View style={styles.itemHeader}>
              <Text style={styles.text}>Fixle</Text>
              <Text style={styles.hour}>09:05PM</Text>
            </View>
            <Text style={{ marginRight: 16, marginTop: 12 }}>We have receive your booking. I will be back shortly to introduce your assigned professional.</Text>
            <View style={styles.button}>
              <Text style={styles.buttonText}>Contact Support</Text>
            </View>
          </View>
        </View>
        <View style={styles.absolute}>
          <Image
            source={images.phone}
            style={{ width: 40, height: 40 }}
          />
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  icon: {
    width: 24,
    height: 24,
  },
  headerText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 18,
    flex: 1,
    marginLeft: 32,
  },
  iconRight: {
    width: 32,
    height: 32,
  },
  headerInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
    marginLeft: 32,
    marginRight: 32,
  },
  infoContainer: {
    marginLeft: 16,
    flex: 1,
    marginRight: 16
  },
  avatar: {
    width: 48,
    height: 48,
    borderRadius: 24
  },
  name: {
    flex: 1,
    color: 'black',
    fontWeight: 'bold',
    fontSize: 14,
  },
  star: {
    width: 14,
    height: 14,
  },
  lineSeparator: {
    height: 1,
    backgroundColor: 'rgba(177,177,177, 0.5)',
    marginTop: 16,
    marginBottom: 24,
  },
  item: {
    flexDirection: 'row',
    marginLeft: 16,
    marginRight: 16,
  },
  itemText: {
    marginLeft: 16,
    marginRight: 16
  },
  itemHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 16,
  },
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black'
  },
  hour: {
    fontSize: 14,
    color: 'grey',
  },
  button: {
    borderRadius: 4,
    backgroundColor: 'rgb(130,115,201)',
    width: 200,
    height: 50,
    justifyContent:'center',
    alignItems:'center',
    marginTop: 20
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  absolute: {
    position: 'absolute',
    width: 56,
    height: 56,
    bottom: 32,
    right: 32,
    backgroundColor: 'rgb(58,54,98)',
    borderRadius: 28,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  }
})
