import React, { Component } from 'react'
import { Dimensions, Text, View, SafeAreaView, StyleSheet, Image, ScrollView } from 'react-native'
import _ from 'lodash'
import { images } from '../../../images';
const { width, height } = Dimensions.get('window')

export default class FirstTabScreen extends Component {

  constructor(props) {
    super(props)
  }

  componentWillMount() {
  }

  shouldComponentUpdate(nextProps, nextState) {
    return true
  }

  render() {
    const { item } = this.props
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
        <ScrollView>
          <View style={styles.header}>
            <Image
              source={images.close}
              style={styles.icon}
            />
            <Text style={styles.headerText}>Electrician</Text>
            <Image
              source={images.option}
              style={styles.iconRight}
            />
          </View>
          <View style={styles.headerInfo}>
            <View style={styles.line}>
              <Image
                source={images.clock}
                style={styles.icon}
              />
              <Text style={styles.clockText}>10:30 AM - 12:30 AM [Fri 08 Sep 2018]</Text>
            </View>
            <View style={[styles.line, { marginTop: 8 }]}>
              <Image
                source={images.marker}
                style={styles.icon}
              />
              <Text style={styles.clockText}>13 Springhill Bottom Rd, TAS, Joonadlup</Text>
            </View>
          </View>
          <Text style={styles.comment}>Comment</Text>
          <Text style={styles.infoText}>I bought it 2 year ago, never cleaning it before, so it has been so dusty and dismissed smell</Text>
          <View style={styles.imageContainer}>
            {arr.map((item, index) =>
              <React.Fragment key={item.toString()}>
                <Image
                  source={{ uri: item }}
                  style={styles.image}
                />
                {index < 3 && <View style={{ width: 12, }} />}
              </React.Fragment>
            )}
          </View>
          <View style={styles.lineSeparator} />
          <Text style={styles.title}>Estimate</Text>
          <View style={styles.billRow}>
            <Text style={styles.itemText}>Minimum change</Text>
            <Text style={styles.price}>$150.00</Text>
          </View>
          <View style={{ height: 20 }} />
          <View style={styles.billRow}>
            <Text style={styles.itemText}>10% GST</Text>
            <Text style={styles.price}>$15.00</Text>
          </View>
          <View style={styles.lineSeparator} />
          <View style={{ height: 16 }} />
          <View style={styles.billRow}>
            <Text style={styles.total}>Total</Text>
            <Text style={styles.totalPrice}>$165.00</Text>
          </View>
          <View style={{ height: 16 }} />
          <View style={styles.lineSeparator} />
          <Text style={[styles.infoText, { color: 'rgba(177,177,177, 1)', marginTop: 10 }]}>If required, any additional parts or labour hours will be dicussed onsite before we commence the job</Text>
          <View style={{ height: 16 }} />
        </ScrollView>
        <View style={styles.absolute}>
          <Image
            source={images.phone}
            style={{ width: 40, height: 40 }}
          />
        </View>
      </SafeAreaView>)
  }

}

const arr = [
  'https://cdnimages.familyhomeplans.com/plans/75977/75977-b1200.jpg',
  'https://pmcvariety.files.wordpress.com/2018/07/bradybunchhouse_sc11.jpg?w=1000&h=563&crop=1',
  'https://cdn.vox-cdn.com/thumbor/0__zWQZmmmwHA5OjBTAchz6_sBw=/0x0:3000x2000/1200x800/filters:focal(1260x760:1740x1240)/cdn.vox-cdn.com/uploads/chorus_image/image/62922957/4854_Alonzo_Ave__Encino_FInals_34.0.jpg',
  'https://cplusc.com.au/wp-content/uploads/2018/05/Iron-Maiden-House_Front-Facade-Low.jpg',
]

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  icon: {
    width: 24,
    height: 24,
  },
  headerText: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 18,
    flex: 1,
    marginLeft: 32,
  },
  iconRight: {
    width: 32,
    height: 32,
  },
  line: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
  },
  headerInfo: {
    paddingTop: 8,
    paddingBottom: 16,
    backgroundColor: 'rgb(58,54,98)',
  },
  clockText: {
    fontSize: 16,
    color: 'white',
    marginLeft: 24
  },
  comment: {
    color: 'rgb(177,177,177)',
    fontSize: 16,
    marginLeft: 16,
    marginVertical: 24
  },
  infoText: {
    color: 'black',
    fontSize: 14,
    marginHorizontal: 16
  },
  image: {
    width: (width - 68) / 4,
    height: 64,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: 'rgba(177,177,177, 0.5)',
  },
  imageContainer: {
    flexDirection: 'row',
    marginHorizontal: 16,
    marginVertical: 24,
  },
  lineSeparator: {
    height: 1,
    backgroundColor: 'rgba(177,177,177, 0.5)',
  },
  title: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 16,
    marginVertical: 24,
  },
  price: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 18,
  },
  billRow: {
    flexDirection: 'row',
    paddingHorizontal: 16,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  itemText: {
    color: 'black',
    fontSize: 14,
  },
  total: {
    color: 'black',
    fontSize: 14,
    fontWeight: 'bold'
  },
  totalPrice: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20,
  },
  absolute: {
    position: 'absolute',
    width: 56,
    height: 56,
    bottom: 32,
    right: 32,
    backgroundColor: 'rgb(58,54,98)',
    borderRadius: 28,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
  }
})
